import policy, env, nodes
import gym
from copy import deepcopy
import numpy as np
import matplotlib.pyplot as plt

episodes =10
rewards = []
moving_average = []

'''
This is for experimenting the implementation:
- play certain number of episodes(rounds) of the game
- for deciding each move to play at each step, MCTS algorithm will be applied
- collect the plot the rewards to check if the MCTS is actually working
- For CartPole-v1, in particualr, 200 is the maximum possible reward.
'''

for episode in range(episodes):
    reward_episode = 0
    game = gym.make(env.GAME_NAME)
    observation = game.reset()
    done = False

    new_game = deepcopy(game)
    mytree = nodes.Node(new_game, False, 0, observation, 0)

    print('episode #' + str(episode+1))

    while not done:
        mytree, action = policy.Policy_Player_MCTS(mytree)
        observation, reward, done, _, _ = game.step(action)
        reward_episode = reward_episode + reward

        # This line is for checking agent in action
        #game.render()

        if done:
            print('reward_episode: ' + str(reward_episode))
            game.close()
            break

    rewards.append(reward_episode)
    moving_average.append(np.mean(rewards[-100:]))

plt.plot(rewards)
plt.plot(moving_average)
plt.show()
print('moving average: ' + str(np.mean(rewards[-20:])))