from math import *
from copy import deepcopy
import random
import env

class Node:
    '''
    Node of MCTS tree
    '''
    def __init__(self, game, done, parent, observation, action_index):
        # child node - next state
        self.child = None

        # total rewards from MCTS exploration - sum of the rollouts that have been started from current node (state)
        self.T = 0

        # visit count
        self.N = 0

        # the environment (copy of the original game for simulation) - current state of the game
        self.game = game

        # observation of the environment - current state of the game
        self.observation = observation

        # if the result (win/draw/loss) is acquired - to stop the search further
        self.done = done

        # link to parent node - for backpropagation
        self.parent = parent

        # action index that leads to this node - is the action that the parent of the node has taken to get into this node
        self.action_index = action_index


    def getUCBscore(self):
        '''
        formula for a value to the node
        MCTS will pick the nodes with the highest value.
        '''

        # set inf for unexplored nodes
        if self.N == 0:
            return float('inf')
        
        # get parent node
        top_node = self
        if top_node.parent:
            top_node = top_node.parent

        # Formula of UCB(Upper Confidence Bound)
        # (self.T / self.N) + c * sqrt(log(top_node.N) / self.N)
        # where c is tunable constant
        return (self.T / self.N) + 0.5 * sqrt(log(top_node.N) / self.N)
    
    def detach_parent(self):
        del self.parent
        self.parent = None


    def create_child(self):
        '''
        Create one child for each possible action of the game
        then, apply such action to a copy of the current node environment
        and create such child node with proper information returned from the action executed
        '''

        if self.done:
            return
        
        actions = []
        games = []
        # create child node with possible action from GAME_ACTIONS
        for i in range(env.GAME_ACTIONS):
            actions.append(i)
            new_game = deepcopy(self.game)
            games.append(new_game)

        child = {}
        for action, game in zip(actions, games):
            observation, reward, done, _, _ = game.step(action)
            child[action] = Node(game, done, self, observation, action)

        self.child = child
        

    def explore(self):
        '''
        The search along the tree is as follows:
        - from the current node, recursively pick the child which maximizes the value according to the MCTS formula
        - when a leaf is reached: 
            - if it has never been explored before, do a rollout and update its current value
            - otherwise, expand the node creating its children, pick one child at random, do a rollout and update its value
        -backpropagate the updated statistics up the tree until the root: update both value and visit counts
        '''
        
        # find a leaf node by choosing the node with max U where U = UCBscore
        current = self

        while current.child:
            child = current.child
            max_U = max(c.getUCBscore() for c in child.values())
            actions = [a for a,c in child.items() if c.getUCBscore() == max_U]
            if len(actions) == 0:
                print("error zero length ", max_U)

            action = random.choice(actions)
            current = child[action]

        #play random game, or expand if needed
        # if never visited, do a rollout and add it to current reward (update)
        if current.N < 1:
            current.T = current.T + current.rollout()

        # if the leaf is visited, create children and choose one child randomly. Then, do a rollout and update
        else:
            current.create_child()
            if current.child:
                current = random.choice(current.child)
            current.T = current.T + current.rollout()

        # update the visit count
        current.N += 1

        # update statistics and backpropagate

        parent = current

        while parent.parent:
            parent = parent.parent
            parent.N += 1
            parent.T = parent.T + current.T

    def rollout(self):
        '''
        The rollout is a random play from a copy of the environment of the current node using random moves.
        This will give us a value for the current node.
        Taken alone, this value is quite random, but, the more rollouts we will do for such node,
        the more accurate the average of the value for such node will be. This is at the core of the MCTS algorithm
        '''

        if self.done:
            return 0
        
        v = 0
        done = False
        new_game = deepcopy(self.game)
        while not done:
            action = new_game.action_space.sample()
            observation, reward, done, _, _ = new_game.step(action)
            v = v + reward
            if done:
                new_game.reset()
                new_game.close()
                break
        return v

    def next(self):
        '''
        Once we have done enough search in teh tree, the values contained in it should be statistically accurate
        At some point, next action to play from the current node will be chosen
        The strategy for choosing such action may vary, however, this is the strategy used in this program:
        - pick at random one of the node which has the maximum visit count, as it indicates that it will have a good value anyway.
        '''

        # End the program when the game is done
        if self.done:
            raise ValueError("game has ended")
        
        # There is no child, but game is not done
        if not self.child:
            raise ValueError('no children found and game hasn\'t ended.')
        
        child = self.child

        # Find node with maximum visit count
        max_N = max(node.N for node in child.values())
        max_children = [c for a, c in child.items() if c.N == max_N]

        # If there is no child with highest visit count
        if len(max_children) == 0:
            print('error zero length ', max_N)

        # Choose one child randomly from the list of children that has max visit count
        max_child = random.choice(max_children)

        return max_child, max_child.action_index

