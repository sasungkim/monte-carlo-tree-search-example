import nodes


MCTS_POLICY_EXPLORE = 100 # MCTS exploring constant: higher = more reliable, slower | this is the time limit of the algorithm (to prevent the brute-force and having nearly infinite time of processing)

def Policy_Player_MCTS(mytree):
    '''
    strategy for using the MCTS:
    - in order to pick the best move from the current node:
        - explore the tree starting from that node for a certain number of iterations to collect reliable statistics
        - pick the node that, according to MCTS, is the best possible next action
    '''

    for i in range(MCTS_POLICY_EXPLORE):
        mytree.explore()
    
    next_tree, next_action = mytree.next()

    # Detach current node and returning the sub-tree that starts from the node rooted at the choosen action
    # The next search, hence, will not start from scratch but will already have collected information and statistics about the nodes,
    # so such statistics can be reused for better reliability
    next_tree.detach_parent()

    return next_tree, next_action


